# boxcom [![Build Status](https://gitlab.com/victor-engmark/boxcom/badges/master/build.svg)](https://gitlab.com/victor-engmark/boxcom)

Toy project which may or may not end up being a mix between a pseudo-realtime strategy and voxel turn-based tactics game.

## Development

### Install dependencies

    make dependencies

### Test

    make test
