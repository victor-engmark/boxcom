XML_EXTENSIONS = iml xml

.PHONY: all
all: build

.PHONY: build
build:
	cargo build

.PHONY: release
release:
	cargo build --release

.PHONY: test
test:
	cargo test

.PHONY: lint
lint:
	find . -name '*.rs' -exec rustfmt {} +
	cargo clippy --all-targets --all-features -- -D warnings

.PHONY: dependencies
dependencies:
	rustup default nightly
	rustup update
	rustup component add clippy-preview

.PHONY: clean
clean:
	$(RM) -r target/

-include make-includes/variables.mk make-includes/xml.mk
